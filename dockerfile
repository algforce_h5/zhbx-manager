
FROM  primetoninc/jdk:1.7
MAINTAINER "Zen Xu"
LABEL Name=zc-service Version=0.0.1
ENV TZ=Asia/Shanghai
ENV LANG C.UTF-8
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ADD manager-0.0.1-SNAPSHOT.jar manager.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/manager.jar"]
