<!DOCTYPE HTML>

<html>
<head>
    <meta charset="utf-8">
    <title>中宏保险</title>
    <meta name="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="full-screen" content="true" />
    <meta name="screen-orientation" content="portrait" />
    <meta name="x5-fullscreen" content="true" />
    <meta name="360-fullscreen" content="true" />
    <style>
        html, body {
            -ms-touch-action: none;
            background: #a6c8b5;
            padding: 0;
            border: 0;
            margin: 0;
            height: 100%;
        }
    </style>
    <script src="${staticResource}/jquery-1.11.0.min.js"></script>
    <script src="${staticResource}/jweixin-1.2.0.js"></script>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?e0fc23cbc76d1ce4208f171983d1e5a4";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
         function GetHMT() {
            return _hmt;
        }
    </script>
</head>
<body>
<div style="margin: auto;width: 100%;height: 100%;" class="egret-player" data-entry-class="Main" data-orientation="auto" data-scale-mode="fixedWidth" data-frame-rate="30"
     data-content-width="750" data-content-height="1334" data-show-paint-rect="false" data-multi-fingered="2" data-show-fps="false"
     data-show-log="false" data-show-fps-style="x:0,y:0,size:12,textColor:0xffffff,bgAlpha:0.9">
</div>
<video style="position: absolute; object-fit: contain;z-index:1;display: none;background-color: black;width: 100%;height: 100%;" id="vd" webkit-playsinline="true" playsinline="true" preload="true" x-webkit-airplay="true" x5-video-player-type="h5" x5-video-player-fullscreen="true"
       src="${staticResource}/resource/assets/vsource.mp4">
</video>
<audio id="bgmic" src="${staticResource}/resource/assets/bgmic.mp3" loop="loop"></audio>
<script>
    var openId ='${openId}';
    function GetOpenId() {
        return openId;
    }

    function playVideo() {
        var vd = document.getElementById("vd");
        return vd;
    }
    function playAudio() {
        var au = document.getElementById("bgmic");
        return au;
    }

    // 微信提供的事件，微信浏览器内部初始化完成后
    document.addEventListener("WeixinJSBridgeReady", function () {
        var au = document.getElementById("bgmic");
        au.load();
        var vd = document.getElementById("vd");
        vd.load();
    }, false);

    function onBridgeReady(){
        WeixinJSBridge.call('showOptionMenu'); //显示菜单
        //WeixinJSBridge.call('hideOptionMenu');  //隐藏菜单
    }

    $(document).ready(function () {
        function getUserInfo() {
            $.ajax({
                type: "POST",  //提交方式
                contentType: "application/x-www-form-urlencoded",
                url: "./wechat/signal",//路径
                data: 'url=' + encodeURIComponent(location.href.split('#')[0]),
                success: function (result) {//返回数据根据结果进行相应的处理
                    if (result.sucflag) {
                        configWeChat(result);
                    }
                }
            });
        }

        $.ajax({
            type: "POST",  //提交方式
            contentType: "application/x-www-form-urlencoded",
            url: "./statistics/logininfo",//路径
            data: 'salesmanId=' + GetQueryString('id') + "&openId=" + '${openId}' + "&parentId=" + GetQueryString('parentId')
        });

        function configWeChat(result) {
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: result.appId, // 必填，公众号的唯一标识
                timestamp: result.timestamp, // 必填，生成签名的时间戳
                nonceStr: result.nonceStr, // 必填，生成签名的随机串
                signature: result.signature,// 必填，签名，见附录1
                jsApiList: ["onMenuShareTimeline",
                    "onMenuShareAppMessage",
                    "onMenuShareQQ",
                    "onMenuShareWeibo"] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
            });

            wx.error(function (res) {
                // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
            });

            var activilink=ChangeParam("parentId",'${openId}');
            var _title = '自主人生，不用等！';
            var _link = activilink;
            var _imgUrl = '${staticResource}/icon.jpg';
            var _desc = '只有拼出来的成功，没有等出来的辉煌。'

            wx.ready(function () {
                wx.onMenuShareTimeline({
                    title: _title, // 分享标题
                    link: _link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                    imgUrl: _imgUrl, // 分享图标
                    success: function () {
                        $.ajax({
                            type: "POST",  //提交方式
                            contentType: "application/x-www-form-urlencoded",
                            url: "./statistics/share/timeline",//路径
                            data: 'openId=' + '${openId}'
                        });
                    },

                    cancel: function () {
                    }
                });

                wx.onMenuShareAppMessage({
                    title: _title, // 分享标题
                    desc: _desc, // 分享描述
                    link: _link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                    imgUrl: _imgUrl, // 分享图标
                    type: '', // 分享类型,music、video或link，不填默认为link
                    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                    success: function () {
                        $.ajax({
                            type: "POST",  //提交方式
                            contentType: "application/x-www-form-urlencoded",
                            url: "./statistics/share/single",//路径
                            data: 'openId=' + '${openId}'
                        });
                    },
                    cancel: function () {
                    }
                });

                wx.onMenuShareQQ({
                    title: _title, // 分享标题
                    desc: _desc, // 分享描述
                    link: _link, // 分享链接
                    imgUrl: _imgUrl, // 分享图标
                    success: function () {
                    },
                    cancel: function () {
                    }
                });

                wx.onMenuShareWeibo({
                    title: _title, // 分享标题
                    desc: _desc, // 分享描述
                    link: _link, // 分享链接
                    imgUrl: _imgUrl, // 分享图标
                    success: function () {
                    },
                    cancel: function () {
                    }
                });

                if (typeof WeixinJSBridge == "undefined"){
                    if( document.addEventListener ){
                        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                    }else if (document.attachEvent){
                        document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
                    }
                }else{
                    onBridgeReady();
                }
            });
        }
        getUserInfo();
    });

    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return r[2]; return null;
    }

    function ChangeParam(name, value) {
        var url = window.location.href;
        var newUrl = "";
        var reg = new RegExp("(^|)" + name + "=([^&]*)(|$)");
        var tmp = name + "=" + value;
        if (url.match(reg) != null) {
            newUrl = url.replace(eval(reg), tmp);
        }
        else {
            if (url.match("[\?]")) {
                newUrl = url + "&" + tmp;
            }
            else {
                newUrl = url + "?" + tmp;
            }
        }

        return newUrl;
    }

    var loadScript = function (list, callback) {
        var loaded = 0;
        var loadNext = function () {
            loadSingleScript(list[loaded], function () {
                loaded++;
                if (loaded >= list.length) {
                    callback();
                }
                else {
                    loadNext();
                }
            })
        };
        loadNext();
    };

    var loadSingleScript = function (src, callback) {
        var s = document.createElement('script');
        s.async = false;
        s.src = src;
        s.addEventListener('load', function () {
            s.parentNode.removeChild(s);
            s.removeEventListener('load', arguments.callee, false);
            callback();
        }, false);

        document.body.appendChild(s);
    };

    <#--var list = new Array();-->
    <#--var staticResource = "${staticResource}";-->
    <#--list[0]=staticResource+"/libs/modules/egret/egret.min.js";-->
    <#--list[1]=staticResource+"/libs/modules/egret/egret.web.min.js";-->
    <#--list[2]=staticResource+"/libs/modules/game/game.min.js";-->
    <#--list[3]=staticResource+"/libs/modules/res/res.min.js";-->
    <#--list[4]=staticResource+"/libs/modules/eui/eui.min.js";-->
    <#--list[5]=staticResource+"/libs/modules/tween/tween.min.js";-->
    <#--list[6]=staticResource+"/particle/particle.min.js";-->
    <#--list[7]=staticResource+"/promise/bin/promise.min.js";-->
    <#--list[8]=staticResource+"/main.min.js";-->

    <#--loadScript(list, function () {-->
        <#--/**-->
         <#--* {-->
             <#--* "renderMode":, //Engine rendering mode, "canvas" or "webgl"-->
             <#--* "audioType": 0 //Use the audio type, 0: default, 2: web audio, 3: audio-->
             <#--* "antialias": //Whether the anti-aliasing is enabled in WebGL mode, true: on, false: off, defaults to false-->
             <#--* "retina": //Whether the canvas is based on the devicePixelRatio-->
             <#--* }-->
         <#--**/-->
        <#--egret.runEgret({ renderMode: "canvas", audioType: 0,retina:true });-->
    <#--});-->
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './manifest.json?v=' + Math.random(), true);
    xhr.addEventListener("load", function () {
        var manifest = JSON.parse(xhr.response);
        var list = manifest.initial.concat(manifest.game);
        loadScript(list, function () {
            /**
             * {
             * "renderMode":, //Engine rendering mode, "canvas" or "webgl"
             * "audioType": 0 //Use the audio type, 0: default, 2: web audio, 3: audio
             * "antialias": //Whether the anti-aliasing is enabled in WebGL mode, true: on, false: off, defaults to false
             * "retina": //Whether the canvas is based on the devicePixelRatio
             * }
             **/
            egret.runEgret({ renderMode: "canvas", audioType: 0 });
        });
    });
    xhr.send(null);


</script>
</body>
</html>