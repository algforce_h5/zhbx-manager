$(function(){
    //取地址栏
    var userphone = $.query.get("rd");
    //获取营销员的信息
    getSales = function(salesmanphone){
        $.ajax({
            url:"/relatrd/"+salesmanphone,
            type: 'GET',
            success: function (data, status, xhr) {
                //填充数据
                var salesRelatrd = data;
                $("#salesmanName").html(salesRelatrd.name);
            },
            error:function(xhr,status,data){
                //将错误信息取出来
                var responseText = $.evalJSON(xhr.responseText);
                alert(responseText.message);
              }
        });
    }
    //离开事件
    bindgetSales = function(){
        var salesmanphone = $("#salesmanphone").val();
        if(salesmanphone.length == 11){
            getSales(salesmanphone);            
        }else{
            $("#salesmanName").html("");
        }
    }
    //保存数据
    saveForm = function(){
        var salesmanphone = $("#salesmanphone").val();
        if(salesmanphone == undefined && salesmanphone == ""){
            alert("手机号码不能为空!");            
            return false;
        }
        var checkPhone = /^1[3|4|5|7|8]\d{9}$/;
        if(!checkPhone.test(salesmanphone)) {
            alert("请输入正确的手机号码!");
            return false;            
        }
        var dataparam = {"salesmanphone":salesmanphone,"userphone":userphone};
        $.ajax({
            url:"/relatrd",
            type: 'POST',  
            contentType : 'application/json', 
            async:false,            
            data:$.toJSON(dataparam),            
            success: function (data, status, xhr) {
                window.location.href="http://zhbxll.esmartwave.com";
            },
            error:function(xhr,status,data){
                //将错误信息取出来
                var responseText = $.evalJSON(xhr.responseText);
                alert(responseText.message);
            }
        });
    }
    //保存事件
    $("#save").click(function(){
        saveForm();
    });
    

})