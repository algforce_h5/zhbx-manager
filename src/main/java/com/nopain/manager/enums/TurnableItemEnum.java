package com.nopain.manager.enums;

/**
 * Created by zenof on 2017/10/29.
 */
public enum TurnableItemEnum {

    NONE(1, "未中奖", "0-4999"),
    PHONE_FLOW(2, "免费流量包", "5000-9983"),
    CAMERA(3, "富士拍立得", "9984-9998"),
    IPHONEX(4, "IPhoneX", "9999-9999");


    private Integer itemNo;
    private String itemName;
    private String itemRange;

    TurnableItemEnum(Integer itemNo, String itemName, String itemRange) {
        this.itemNo = itemNo;
        this.itemName = itemName;
        this.itemRange = itemRange;
    }

    public Integer getItemNo() {
        return itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public String getItemRange() {
        return itemRange;
    }
}
