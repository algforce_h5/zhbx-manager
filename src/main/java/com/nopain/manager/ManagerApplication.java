package com.nopain.manager;

import com.nopain.manager.config.DDHSetting;
import com.nopain.manager.config.DataSourceSetting;
import com.nopain.manager.config.SalesmanSearchSetting;
import com.nopain.manager.config.WeChatSetting;
import com.nopain.manager.util.DBUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@EnableConfigurationProperties({WeChatSetting.class, DDHSetting.class, DataSourceSetting.class, SalesmanSearchSetting.class})
@PropertySource(value = {"classpath:/zhonghong.properties","${external.properties.location}"}, ignoreResourceNotFound = true)
@SpringBootApplication
public class ManagerApplication extends SpringBootServletInitializer {

    @Autowired
    private DataSourceSetting dataSourceSetting;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ManagerApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ManagerApplication.class, args);
    }


    @Bean
    @ConfigurationProperties("algforce.zh.datasource")
    public DataSource dataSource() {
        String dbk=dataSourceSetting.getDbk();
        String username = dataSourceSetting.getUsrname();
        String deUsername= DBUtil.Decrypt(username,dbk);
        String pwd = dataSourceSetting.getPwd();
        String dePwd=DBUtil.Decrypt(pwd,dbk);
        return DataSourceBuilder.create().username(deUsername).password(dePwd).build();
    }
}
