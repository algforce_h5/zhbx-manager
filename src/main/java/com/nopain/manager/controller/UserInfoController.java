package com.nopain.manager.controller;

import com.nopain.manager.config.DDHSetting;
import com.nopain.manager.config.Response;
import com.nopain.manager.entity.SMSInfo;
import com.nopain.manager.entity.UserInfo;
import com.nopain.manager.enums.TurnableItemEnum;
import com.nopain.manager.repository.SMSInfoRepository;
import com.nopain.manager.repository.UserInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLDecoder;
import java.text.ParseException;
import java.util.*;


/**
 * 用户信息Controller
 */
@RestController
@RequestMapping("/user")
public class UserInfoController {
    public static Logger logger = LoggerFactory.getLogger(UserInfoController.class);
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private SMSInfoRepository smsInfoRepository;

    @Autowired
    private DDHSetting ddhSetting;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> save(String openId, String userName, String phone, String code,Integer flag) {
        Map<String, Object> data = new HashMap<>();
        data.put("sucflag", true);
        try {
            String deOpenId = URLDecoder.decode(openId, "UTF-8");
            String deUserName = URLDecoder.decode(userName, "UTF-8");
            String dePhone = URLDecoder.decode(phone, "UTF-8");
            String deCode = URLDecoder.decode(code, "UTF-8");

            if (StringUtils.isEmpty(deOpenId)) {
                data.put("sucflag", false);
                data.put("message", "信息填写不完全");
                data.put("err",1);
                return Response.success(data);
            } else if (StringUtils.isEmpty(deUserName) || deUserName.length() > 200) {
                data.put("sucflag", false);
                data.put("message", "姓名填写错误");
                data.put("err",1);
                return Response.success(data);
            } else if (StringUtils.isEmpty(dePhone)) {
                data.put("sucflag", false);
                data.put("message", "电话填写错误");
                data.put("err",1);
                return Response.success(data);
            } else if (StringUtils.isEmpty(deCode) || deCode.length() != 6) {
                data.put("sucflag", false);
                data.put("message", "验证码填写错误");
                data.put("err",1);
                return Response.success(data);
            }else if(flag==null || flag.equals(0)){
                data.put("sucflag", false);
                data.put("message", "参数错误");
                data.put("err",1);
                return Response.success(data);
            }
            if(ddhSetting.getEnv().equals("1")||ddhSetting.getEnv().equals("2")){
                if(!deCode.equals("111111")){
                    data.put("sucflag", false);
                    data.put("message", "验证码不匹配");
                    data.put("err",1);
                    return Response.success(data);
                }
            }else{
                List<SMSInfo> smsInfos = smsInfoRepository.findByMoblPhonOrderByInsertTimeDesc(dePhone);
                if (smsInfos == null || smsInfos.size() == 0 || !deCode.equals(smsInfos.get(0).getCode())) {
                    data.put("sucflag", false);
                    data.put("message", "验证码不匹配");
                    data.put("err",1);
                    return Response.success(data);
                }
            }

//            List<UserInfo> oldUserInfos = userInfoRepository.findByOpenId(deOpenId);

            long openIdCount=userInfoRepository.countByOpenId(deOpenId);
            if (openIdCount > 3) {
                data.put("sucflag", false);
                data.put("message", "用户抽奖次数已经满了");
                data.put("err",4);
                return Response.success(data);
            }

            long phoneCount=userInfoRepository.countByOpenIdAndPhone(deOpenId,dePhone);
            if(phoneCount>1){
                data.put("sucflag", false);
                data.put("message", "手机号抽奖次数已经满了");
                data.put("err",2);
                return Response.success(data);
            }

            UserInfo userInfo = new UserInfo();
            userInfo.setCreateTime(new Date());

            userInfo.setOpenId(deOpenId);
            userInfo.setUserName(deUserName);
            userInfo.setPhone(dePhone);
            userInfo.setActionNo(flag);
            String actionName="";
            if(flag.equals(1)){
                actionName="积极进取乐天派";
            }else if(flag.equals(2)){
                actionName="风驰电掣行动派";
            }else if(flag.equals(3)){
                actionName="百折不挠修仙派";
            }
            userInfo.setActionName(actionName);
            userInfo.setCreateTime(new Date());

            //抽奖
            TurnableItemEnum turnableItemEnum = getItem();
            userInfo.setItemName(turnableItemEnum.getItemName());
            userInfo.setItemNo(turnableItemEnum.getItemNo());
            data.put("itemno", turnableItemEnum.getItemNo());
            data.put("itemname", turnableItemEnum.getItemName());
            logger.info("itemno:" + turnableItemEnum.getItemNo());
            userInfoRepository.save(userInfo);
            logger.info("提交成功");
            return Response.success(data);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("服务器出错");
            data.put("sucflag", false);
            data.put("message", "服务器出错");
            data.put("err",1);
            return Response.success(data);
        }
    }

    /**
     * 发送验证码
     *
     * @param phone
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/code")
    public ResponseEntity sendCode(String phone) {
        Map<String, Object> data = new HashMap<>();
        try {
            //验证码随机数
            String code = getValiteCode();
            System.out.print(code);
            SMSInfo smsInfo = new SMSInfo();
            smsInfo.setInsertTime(new Date());
            smsInfo.setMoblPhon(phone);
            smsInfo.setMsgTxt("本次中宏保险活动验证码为" + code);
            smsInfo.setMsgType("wechatamo");
            smsInfo.setStatCd("1");
            smsInfo.setCode(code);
            smsInfoRepository.save(smsInfo);
            logger.info("短信验证码发送成功：" + code);
            data.put("sucflag", true);
            data.put("message", "ok");
        } catch (Exception e) {
            data.put("sucflag", false);
            data.put("message", "服务器出错");
            return Response.success(data);
        }
        return Response.success(data);
    }

    /**
     * 获取验证码
     *
     * @return
     */
    protected static String getValiteCode() {
        int code = (int) (Math.random() * 900000 + 100000);
        return String.valueOf(code);
    }


    /**
     * 抽奖概率
     *
     * @return
     */
    private TurnableItemEnum getItem() throws ParseException {
        List<TurnableItemEnum> turnableItemEnums = new ArrayList<TurnableItemEnum>();
        turnableItemEnums.add(TurnableItemEnum.NONE);
        turnableItemEnums.add(TurnableItemEnum.PHONE_FLOW);
        turnableItemEnums.add(TurnableItemEnum.CAMERA);
        turnableItemEnums.add(TurnableItemEnum.IPHONEX);
        Random random = new Random();
        int result = random.nextInt(10000);
        TurnableItemEnum target = TurnableItemEnum.NONE;
        for (TurnableItemEnum item : turnableItemEnums) {
            String itemRange = item.getItemRange();
            String[] ranges = itemRange.split("-");
            if (ranges.length != 2) break;
            if (result >= Integer.parseInt(ranges[0]) && result <= Integer.parseInt(ranges[1])) {
                target = item;
                break;
            }
        }
        if(target.getItemNo().equals(TurnableItemEnum.IPHONEX)){
            long iphoneCount=userInfoRepository.countByItemNo(TurnableItemEnum.IPHONEX.getItemNo());
            if(iphoneCount>=1){
                 target = TurnableItemEnum.NONE;
            }
        }else if(target.getItemNo().equals(TurnableItemEnum.CAMERA)){
            long cameraCount=userInfoRepository.countByItemNo(TurnableItemEnum.CAMERA.getItemNo());
            if(cameraCount>=30){
                target = TurnableItemEnum.NONE;
            }
        }else if(target.getItemNo().equals(TurnableItemEnum.PHONE_FLOW)){
            long phoneFlowCount=userInfoRepository.countByItemNo(TurnableItemEnum.PHONE_FLOW.getItemNo());
            if(phoneFlowCount>=10000){
                target = TurnableItemEnum.NONE;
            }
        }
        return target;
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<UserInfo> findAll() {
        return userInfoRepository.findAll();
    }


}
