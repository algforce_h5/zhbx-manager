package com.nopain.manager.controller;

import com.alibaba.fastjson.JSONObject;
import com.nopain.manager.config.DDHSetting;
import com.nopain.manager.config.DefaultSetting;
import com.nopain.manager.config.WeChatSetting;
import com.nopain.manager.util.AESUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.Enumeration;


/**
 * Created by zenof on 2017/11/14.
 */
@Controller
public class IndexController {

    public static Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private WeChatSetting weChatSetting;

    @Autowired
    private DDHSetting ddhSetting;

    @Autowired
    private DefaultSetting defaultSetting;

    private final String wechatUrl = "https://open.weixin.qq.com/connect/oauth2/authorize";

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String findDetailByPage(String data, String sign, Model model, HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            data = data.replace(" ", "+");
            sign = sign.replace(" ", "+");
            String openId = (String) session.getAttribute("openId");
            String logStr=String.format("post_openId=%s,post_sessionID=%s,post_openId==null=%s",openId,session.getId(),(openId == null));
            logger.info(logStr);
            model.addAttribute("staticResource", ddhSetting.getStaticresource());
            if (openId == null) {
                AESUtil aesUtil = new AESUtil();
                String[] decryptStr = new String[1];
                decryptStr[0] = aesUtil.decrypt(data);

                JSONObject object = JSONObject.parseObject(decryptStr[0]);
                decryptStr[0] = "";
                Object openIdObj = object.get("openid");
                logger.info("openId=" + openIdObj.toString());
                model.addAttribute("openId", openIdObj.toString());
                session.setAttribute("openId", openId);
            } else {
                model.addAttribute("openId", openId);
                session.setAttribute("openId", openId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("openId", "");
            logger.info("系统异常");
        }
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String findDetailByPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        HttpSession session = request.getSession();
        try {
            String openId = (String) session.getAttribute("openId");
            String logStr=String.format("get_url=%s,get_openId=%s,get_sessionID=%s",openId,openId,session.getId());
            logger.info(logStr);
            if(!weChatSetting.getOn()){
                model.addAttribute("staticResource",ddhSetting.getStaticresource());
                model.addAttribute("openId", "123");//debug only
                return "index";
            }
            if( StringUtils.isEmpty(openId)){
                String queryString = "";
                Enumeration<String> paramNames = request.getParameterNames();//获取所有的参数名
                while (paramNames.hasMoreElements()) {
                    if(!StringUtils.isEmpty(queryString) ){
                        queryString+="&";
                    }
                    String name = paramNames.nextElement();//得到参数名
                    String value = request.getParameter(name);//通过参数名获取对应的值
                    queryString += String.format("%s=%s", name, value);
                }
                String url = request.getRequestURL().toString();
                if(!url.contains("https")){
                    url=url.replace("http","https");
                }
                if(!StringUtils.isEmpty(queryString)){
                    url+= "?" + queryString;
                }
                String linkUrl = weChatSetting.getAuthorize() + url;
                String encodeUrl = URLEncoder.encode(linkUrl, "utf-8");
                String resultUrl =String.format("%s?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=wechatCampaign3#wechat_redirect",wechatUrl, weChatSetting.getAppid(),encodeUrl);
                //Logger.getLogger("IndexController").info("url=" + resultUrl);
                return  String.format("redirect:%s",resultUrl) ;
            }else{
                model.addAttribute("staticResource",ddhSetting.getStaticresource());
                model.addAttribute("openId", openId);
                session.setAttribute("openId",openId);
                return "index";
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("系统异常");
            session.setAttribute("openId","");
            model.addAttribute("openId", "");
            return "index";
        }
    }
    /**
     * 绑定用户手机
     */
    @RequestMapping(value = "/rd/{phonecode}", method = RequestMethod.GET)
    public void index(HttpServletResponse response, @PathVariable String phonecode) {
        try {
//            /**
//             * 转义电话号码
//             * 电话号码是x3+2的
//             * 反向操作
//             */
//            phonecode = new String(Base64Utils.decodeFromString(phonecode),"UTF-8");
//            long longphone = (Long.parseLong(phonecode)-2)/3;
            //重定向到一个静态页
            String url = defaultSetting.getReditUrl()+String.valueOf(phonecode);
            response.sendRedirect(url);
        } catch (Exception e) {
            logger.error("错误：" + e.getMessage());
        }
    }
}
