package com.nopain.manager.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nopain.manager.config.DDHSetting;
import com.nopain.manager.config.Response;
import com.nopain.manager.config.SalesmanSearchSetting;
import com.nopain.manager.config.WeChatSetting;
import com.nopain.manager.entity.AgtInfo;
import com.nopain.manager.entity.SalesInfo;
import com.nopain.manager.repository.AgtInfoRepository;
import com.nopain.manager.repository.SalesInfoRepository;
import com.nopain.manager.util.DateUtil;
import com.nopain.manager.util.ENamCardUtil;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 销售信息Controller
 */
@RestController
@RequestMapping("/sales")
public class SalesInfoController {
    public static Logger logger = LoggerFactory.getLogger(SalesInfoController.class);
    @Autowired
    private SalesInfoRepository salesInfoRepository;

    @Autowired
    private WeChatSetting weChatSetting;

    @Autowired
    private DDHSetting ddhSetting;

    @Autowired
    private SalesmanSearchSetting salesmanSearchSetting;

    @Autowired
    private AgtInfoRepository agtInfoRepository;

    @RequestMapping(value = "/enamecard", method = RequestMethod.POST)
    public ResponseEntity<Object> findEnameCard(String salesmanId) {
        logger.info("salesmanId=" + salesmanId);
        Map<String, Object> data = new HashMap<>();
        data.put("sucflag", true);

        //如果本地电子名片能获得数据则直接返回，否则从接口获取电子名片
        if(data!=null){
            return Response.success(data);
        }
        try {
            HttpHeaders headers = new HttpHeaders();
            MediaType type = MediaType.parseMediaType("application/xml; charset=UTF-8");
            headers.setContentType(type);
            headers.add("Accept", MediaType.APPLICATION_XML.toString());
            String salescode = salesmanSearchSetting.getSalescode();
            logger.info("配置code:" + salescode);
            if (salescode == null) {
                List<SalesInfo> salesInfos = salesInfoRepository.findBySalesmanId(salesmanId);
                SalesInfo salesInfo = null;
                if (salesInfos == null || salesInfos.size() == 0) {
                    logger.info("本地salesInfos为空,需要查询saleInfo");
                    salesInfo = findSaleInfo(salesmanId);
                    if (salesInfo == null) {
                        data.put("sucflag", false);
                        data.put("message", "对象不存在");
                        return Response.success(data);
                    }
                } else {
                    salesInfo = salesInfos.get(0);
                }
                //调用enamcard接口
                salescode = salesInfo.getSalesmanCode();
                logger.info("salescode" + salescode);
            }
            data=getLocalAgtInfo(salescode);
            if(data!=null){
                logger.info("电子名片本地获取成功");
                return Response.success(data);
            }
            String req = ENamCardUtil.getRequest(salescode);
            logger.info("req" + req);
            HttpEntity<String> formEntity = new HttpEntity<String>(req, headers);
            RestTemplate restTemplate = new RestTemplate();
            String resutl = restTemplate.postForObject(weChatSetting.getEnamecard(), formEntity, String.class);
            logger.info("resutl" + resutl);
            data = ENamCardUtil.getVal(resutl);
            logger.info("enamecard=" + data);
            if(data==null){
                logger.info("电子名片返回为null");
                data.put("sucflag", false);
                data.put("message", "电子名片返回为null");
            }else{
                logger.info("创建并更新agtinfo电子名片");
                //创建并更新agtinfo电子名片
                createAndUpdateAgtInfo(data,salesmanId,salescode);
            }
            return Response.success(data);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("sucflag", false);
            data.put("message", "服务器出错");
            logger.info("无法获取电子名片");
            return Response.success(data);
        }
    }

    private void createAndUpdateAgtInfo(Map<String,Object> data,String salesmenId,String salescode){
        List<AgtInfo> agtInfoList=agtInfoRepository.findBySalesmenId(salesmenId);
        AgtInfo agtInfo=new AgtInfo();
        if(agtInfoList!=null && agtInfoList.size()>0){
            agtInfo=agtInfoList.get(0);
        }
        agtInfo.setSalesmenId(salesmenId);
        agtInfo.setAgtAddress(data.get("address").toString());
        agtInfo.setAgtCompany(data.get("company").toString());
        agtInfo.setAgtMobile(data.get("phone").toString());
        agtInfo.setAgtName(data.get("name").toString());
        agtInfo.setAgtRank(data.get("rank").toString());
        agtInfo.setAgtTel(data.get("tel").toString());
        agtInfo.setAgtCD(salescode);
        agtInfo.setAgtPhoto("");
        agtInfoRepository.save(agtInfo);
    }

    public Map<String, Object> getLocalAgtInfo(String agtCode) {
        Map<String, Object> data = new HashMap<>();
        data.put("sucflag", true);
        List<AgtInfo> agtInfoList = agtInfoRepository.findByAgtCD(agtCode);
        if (agtInfoList == null || agtInfoList.size() < 1) {
           return null;
        }
        AgtInfo agtInfo = agtInfoList.get(0);
        data.put("address", agtInfo.getAgtAddress().replace("<br/>",""));
        String agtCompStr = "中宏人寿保险有限公司";
        data.put("company", agtCompStr);
        data.put("phone", agtInfo.getAgtMobile());
        data.put("name", agtInfo.getAgtName());
        data.put("tel", agtInfo.getAgtTel());
        data.put("photo", agtInfo.getAgtPhoto());
        data.put("rank", agtInfo.getAgtRank());
        return data;

    }


    @RequestMapping(value = "/notification", method = RequestMethod.POST)
    public ResponseEntity<Object> notificationPost(String um, String pd, String data) throws UnsupportedEncodingException {
        return notificationCore(um, pd, data);
    }

    @RequestMapping(value = "/notification", method = RequestMethod.GET)
    public ResponseEntity<Object> notificationGet(String um, String pd, String data) throws UnsupportedEncodingException {
        return notificationCore(um, pd, data);
    }

    private ResponseEntity<Object> notificationCore(String um, String pd, String data) throws UnsupportedEncodingException {
        Map<String, Object> returnObj = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        if (StringUtils.isEmpty(data)) {
            logger.info("notification数据空");
            setResult(-1, "err", returnObj, result);
            return Response.success(result);
        }
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] deByte = null;
        try {
            deByte = decoder.decodeBuffer(data);
        } catch (IOException e) {
            logger.info("notificatio解码异常");
            e.printStackTrace();
        }
        try {
            String deData = new String(deByte, "utf-8");
            SalesInfo target = JSONObject.parseObject(deData, SalesInfo.class);
            if (target == null) {
                setResult(102, "解析异常", returnObj, result);
                logger.info("notification JSON解析异常");
                return Response.success(result);
            }
            List<SalesInfo> salesInfos = salesInfoRepository.findBySalesmanId(target.getSalesmanId());
            SalesInfo salesInfo = null;
            if (salesInfos != null && salesInfos.size() > 0) {
                salesInfo = salesInfos.get(0);
                target.copyTo(salesInfo);
            } else {
                salesInfo = target;
            }
            salesInfoRepository.save(salesInfo);

            List<AgtInfo>  agtInfoList= agtInfoRepository.findByAgtCD(salesInfo.getSalesmanCode());
            for (AgtInfo agtInfo :agtInfoList){
                agtInfo.setSalesmenId(salesInfo.getSalesmanId());
            }
            agtInfoRepository.save(agtInfoList);
            setResult(0, "ok", returnObj, result);
            logger.info("notification成功：" + result);
            return Response.success(result);
        } catch (Exception e) {
            e.printStackTrace();
            setResult(101, "系统异常", returnObj, result);
            return Response.success(result);
        }

    }

    private void setResult(int errcode, String errmsg, Map<String, Object> returnObj, Map<String, Object> result) throws UnsupportedEncodingException {
        result.put("errcode", errcode);
        result.put("errmsg", errmsg);
    }

    public SalesInfo findSaleInfo(String salesmanId) {
        try {
            Date beginTime = DateUtil.getDate(ddhSetting.startdate);
            Date endTime = DateUtil.getDate(ddhSetting.enddate);
            HttpHeaders headers = new HttpHeaders();
            MediaType type = MediaType.parseMediaType("application/x-www-form-urlencoded; charset=UTF-8");
            headers.setContentType(type);
            headers.add("Accept", MediaType.APPLICATION_FORM_URLENCODED.toString());
            JSONObject req = new JSONObject(12);
            req.put("msgid", 1);
            req.put("number", 1);
            req.put("begintime", beginTime.getTime());
            req.put("endtime", endTime.getTime());
            req.put("salesmanId", salesmanId);
            RestTemplate restTemplate = new RestTemplate();
            String um = salesmanSearchSetting.getUm();
            String pd = salesmanSearchSetting.getPd();
            String reqEncrypt = "um=" + um + "&pd=" + pd + "&data=" + Base64Utils.encodeToString(req.toString().getBytes("utf-8"));
            logger.info("查询营销员请求参数：" + reqEncrypt);
            HttpEntity<String> formEntity = new HttpEntity<String>(reqEncrypt, headers);
            ResponseEntity<String> rep = restTemplate.postForEntity(weChatSetting.getSalesmansearch(), formEntity, String.class);
            logger.info("查询营销员返回参数：" + rep.getBody());
            byte[] deByte = Base64Utils.decodeFromString(rep.getBody());
            String deData = new String(deByte, "UTF-8");
            JSONObject target = JSONObject.parseObject(deData);
            JSONArray recordList = JSONArray.parseArray(target.get("recordlist").toString());
            SalesInfo record = JSONObject.parseObject(recordList.get(0).toString(), SalesInfo.class);
            return record;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("查询营销员JSON解析异常");
            return null;
        }
    }


}
