package com.nopain.manager.controller;

import com.nopain.manager.config.Response;
import com.nopain.manager.entity.SalesRelatrd;
import com.nopain.manager.entity.UserInfo;
import com.nopain.manager.entity.UserRelated;
import com.nopain.manager.entity.form.RelatrdForm;
import com.nopain.manager.repository.SalesRelatrdRepository;
import com.nopain.manager.repository.UserInfoRepository;
import com.nopain.manager.repository.UserRelatedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author Glory
 * @version 1.0
 * @time 2017/11/24 10:00
 * @Description TODO 绑定营销员的代码
 */
@RestController
@RequestMapping("/relatrd")
public class UserRelatedController {
    public static Logger logger = LoggerFactory.getLogger(IndexController.class);


    @Autowired
    private UserRelatedRepository userRelatedRepository;
    @Autowired
    private SalesRelatrdRepository salesRelatrdRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;

    /**
     * 保存用户和营销员的信息
     * @param form
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity relatrdUser(@RequestBody RelatrdForm form){
        try {
            /**
             * 转义电话号码
             * 电话号码是x3+2的
             * 反向操作
             */
            String userPhone  = new String(Base64Utils.decodeFromString(form.getUserphone()),"UTF-8");
            userPhone  = String.valueOf((Long.parseLong(userPhone)-2)/3);
            UserRelated userRelated = userRelatedRepository.findOneByUserPhone(userPhone);
            if (userRelated==null){
                //查询用户信息
                UserInfo userInfo =userInfoRepository.findOneByPhone(userPhone);
                //查询营销员信息
                SalesRelatrd salesRelatrd = salesRelatrdRepository.findOneByPhone(form.getSalesmanphone());
                //赋值
                userRelated = new UserRelated();
                userRelated.setUserOpenId(userInfo.getOpenId());
                userRelated.setUserPhone(userInfo.getPhone());
                userRelated.setUserName(userInfo.getUserName());
                userRelated.setSalesmanId(salesRelatrd.getSalesmanId());
                userRelated.setSalesmanCode(salesRelatrd.getSalesmanCode());
                userRelated.setSalesmanPhone(salesRelatrd.getPhone());
                userRelated.setSalesmaName(salesRelatrd.getName());
                userRelated.setCreateTime(new Date());
                userRelatedRepository.save(userRelated);
                return Response.success();
            }else{
                return Response.serverError("请勿重复绑定");
            }
        }catch (Exception e){
            logger.error("错误：" + e.getMessage());
            return Response.serverError("服务器出问题!");
        }

    }
    /**
     * 根据手机号查询营销员的信息
     */
    @RequestMapping(value = "/{phone}",method = RequestMethod.GET)
    public ResponseEntity getSalesInfo(@PathVariable String phone) {
        SalesRelatrd salesRelatrd = salesRelatrdRepository.findOneByPhone(phone);
        if (salesRelatrd!=null){
            return Response.success(salesRelatrd);
        }else{
            return Response.serverError("此号码不存在!");
        }
    }
}
