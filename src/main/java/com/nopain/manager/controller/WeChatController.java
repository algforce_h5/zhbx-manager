package com.nopain.manager.controller;

import com.alibaba.fastjson.JSONObject;
import com.nopain.manager.config.Response;
import com.nopain.manager.config.WeChatSetting;
import com.nopain.manager.entity.WechatInfo;
import com.nopain.manager.repository.WechatInfoRepository;
import com.nopain.manager.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * 微信接口Controller
 */
@RestController
@RequestMapping("/wechat")
public class WeChatController {
    public static Logger logger = LoggerFactory.getLogger(WeChatController.class);
    @Autowired
    private WeChatSetting weChatSetting;

    @Autowired
    private WechatInfoRepository wechatInfoRepository;

    @RequestMapping(value = "/signal", method = RequestMethod.POST)
    public ResponseEntity<Object> getSign(String url) {
        Map<String, Object> data = new HashMap<>();
        try {
            Date currentTime = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentTime);
//            calendar.add(Calendar.HOUR, 1);
            calendar.add(Calendar.MINUTE, weChatSetting.getCalltime());
            Date timeOutDate = calendar.getTime();
            WechatInfo wechatInfo = null;
            List<WechatInfo> wechatInfoList = wechatInfoRepository.findByAppId(weChatSetting.getAppid());
            if (wechatInfoList == null || wechatInfoList.size() < 1 || wechatInfoList.get(0).getExpiredTime().before(currentTime)) {
                Map<String, String> appIdAndTicket = getAppIdAndTicket();
                if (appIdAndTicket == null) {
                    data.put("sucflag", false);
                    logger.info("获取Ticket出错");
                    data.put("message", "获取签名出错");
                    return Response.success(data);
                }
                if (wechatInfoList == null || wechatInfoList.size() < 1) {
                    wechatInfo = new WechatInfo();
                } else {
                    wechatInfo = wechatInfoList.get(0);
                }
                wechatInfo.setAppId(appIdAndTicket.get("appId"));
                wechatInfo.setTicket(appIdAndTicket.get("ticket"));
                wechatInfo.setExpiredTime(timeOutDate);
                wechatInfoRepository.save(wechatInfo);
            } else {
                wechatInfo = wechatInfoList.get(0);
            }
            logger.info("accessToken-appId：" + wechatInfo.getAppId());
            logger.info("accessToken-ticket：" + wechatInfo.getTicket());
            logger.info("accessToken-expiredTime：" + wechatInfo.getExpiredTime());
            //获取签名signature
            String noncestr = UUID.randomUUID().toString();
            String timestamp = Long.toString(System.currentTimeMillis() / 1000);
            String str = "jsapi_ticket=" + wechatInfo.getTicket() +
                    "&noncestr=" + noncestr +
                    "&timestamp=" + timestamp +
                    "&url=" + url;
            //sha1加密
            String signature = SHA1(str);

            logger.info("noncestr=" + noncestr);
            logger.info("timestamp=" + timestamp);
            logger.info("signature=" + signature);
            data.put("sucflag", true);
            data.put("message", "ok");
            data.put("appId", wechatInfo.getAppId());
            data.put("timestamp", timestamp);
            data.put("nonceStr", noncestr);
            data.put("signature", signature);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("sucflag", false);
            data.put("message", "服务器出错");
            logger.info("服务器出错");
        }
        return Response.success(data);

    }

    private Map<String, String> getAppIdAndTicket() throws Exception {
        Map<String, String> data = new HashMap<String, String>();
        String dataStr = "{\"state\":\"wechatCampaign3\"}";
        AESUtil aesUtil = new AESUtil();
        String encryptAES = aesUtil.encrypt(dataStr);

        RSAUtil rsaUtil = new RSAUtil();
        String encryptRSA = rsaUtil.sign(dataStr);
        boolean isValid = rsaUtil.verify(dataStr, encryptRSA);
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/x-www-form-urlencoded; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

        String req = "data=" + encryptAES + "&sign=" + encryptRSA + "&state=wechatCampaign3";
        logger.info("certificate请求:" + req);
        HttpEntity<String> formEntity = new HttpEntity<String>(req.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> rep = restTemplate.postForEntity(weChatSetting.getCertificate(), formEntity, String.class);
        logger.info("certificate返回:" + rep.getBody());
        String resStr = getResponse(rep.getBody());
        if (StringUtils.isEmpty(resStr)) {
            logger.info("resStr为空");
            return null;
        }
        try {
            String resultStr = aesUtil.decrypt(resStr);
            logger.info("certificate解析:" + resultStr);
            JSONObject object = JSONObject.parseObject(resultStr);
            String appid = object.get("appid").toString();
            String ticket = object.get("jsapi_ticket").toString();
            data.put("appId", appid);
            data.put("ticket", ticket);
            logger.info("appid:" + appid);
            logger.info("ticket:" + ticket);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("certificate调用JSTICKET出错");
            return null;
        }
    }

    private String getResponse(String result) {
        if (result == null || !result.contains("&")) {
            return "";
        }
        String dataStr = result.substring(0, result.indexOf("&"));
        String data = dataStr.replace("data=", "");

        return data;
    }


    public static String SHA1(String str) throws NoSuchAlgorithmException {
        try {
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("SHA-1"); //如果是SHA加密只需要将"SHA-1"改成"SHA"即可
            digest.update(str.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexStr = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexStr.append(0);
                }
                hexStr.append(shaHex);
            }
            return hexStr.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


}
