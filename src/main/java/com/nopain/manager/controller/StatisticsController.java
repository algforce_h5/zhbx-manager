package com.nopain.manager.controller;

import com.nopain.manager.config.Response;
import com.nopain.manager.entity.LoginInfo;
import com.nopain.manager.entity.SalesInfo;
import com.nopain.manager.entity.ShareInfo;
import com.nopain.manager.repository.LoginInfoRepository;
import com.nopain.manager.repository.SalesInfoRepository;
import com.nopain.manager.repository.ShareInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 收集信息Controller
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    private LoginInfoRepository loginInfoRepository;

    @Autowired
    private ShareInfoRepository shareInfoRepository;

    @Autowired
    private SalesInfoRepository salesInfoRepository;

    @RequestMapping(value = "/logininfo", method = RequestMethod.POST)
    public ResponseEntity<Object> saveLoginInfo(String salesmanId,String openId,String parentId){
        if(StringUtils.isEmpty(salesmanId)||StringUtils.isEmpty(openId)){
            return Response.success("err");
        }
        List<SalesInfo> salesInfoList= salesInfoRepository.findBySalesmanId(salesmanId);
        if(salesInfoList==null ||salesInfoList.size()==0)  return Response.success("err");
        SalesInfo salesInfo=salesInfoList.get(0);
        LoginInfo loginInfo=new LoginInfo();
        loginInfo.setCreateTime(new Date());
        loginInfo.setOpenId(openId);
        loginInfo.setParentOpenId(parentId);
        loginInfo.setSalesmanCode(salesInfo.getSalesmanCode());
        loginInfo.setSalesmanId(salesInfo.getSalesmanId());
        loginInfo.setSalesmanOpenId(salesInfo.getThirdPartySysId());
        loginInfoRepository.save(loginInfo);
        return   Response.success("ok");
    }

    @RequestMapping(value = "/share/single", method = RequestMethod.POST)
    public ResponseEntity<Object> saveShareInfoBySingle(String openId){
        if(StringUtils.isEmpty(openId)){
            return Response.success("err");
        }
        ShareInfo shareInfo=new ShareInfo();
        shareInfo.setCreateTime(new Date());
        shareInfo.setOpenId(openId);
        shareInfo.setShareType("single");
        shareInfoRepository.save(shareInfo);
        return   Response.success("ok");
    }

    @RequestMapping(value = "/share/timeline", method = RequestMethod.POST)
    public ResponseEntity<Object> saveShareInfoByTimeline(String openId){
        if(StringUtils.isEmpty(openId)){
            return Response.success("err");
        }
        ShareInfo shareInfo=new ShareInfo();
        shareInfo.setCreateTime(new Date());
        shareInfo.setOpenId(openId);
        shareInfo.setShareType("timeline");
        shareInfoRepository.save(shareInfo);
        return   Response.success("ok");
    }

}
