package com.nopain.manager.repository;

import com.nopain.manager.entity.AgtInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zenof on 2017/11/23.
 */
public interface AgtInfoRepository extends JpaRepository<AgtInfo,Long> {

    List<AgtInfo> findBySalesmenId(String salesmenId);

    List<AgtInfo> findByAgtCD(String agtCd);

}
