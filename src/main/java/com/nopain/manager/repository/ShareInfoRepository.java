package com.nopain.manager.repository;

import com.nopain.manager.entity.ShareInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 分享信息数据层
 */
public interface ShareInfoRepository extends JpaRepository<ShareInfo,Long> {
        
}
