package com.nopain.manager.repository;

import com.nopain.manager.entity.LoginInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * 登入信息信息数据层
 */
public interface LoginInfoRepository extends JpaRepository<LoginInfo,Long> {


}
