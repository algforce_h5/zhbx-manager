package com.nopain.manager.repository;

import com.nopain.manager.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 用户信息数据层
 */
public interface UserInfoRepository extends JpaRepository<UserInfo,Long> {

        long countByOpenIdAndPhone(String openId,String phone);

        long countByOpenId(String openId);


        long countByItemNo(Integer Integer);

        UserInfo findOneByPhone(String phone);
}
