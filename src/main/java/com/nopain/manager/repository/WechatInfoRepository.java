package com.nopain.manager.repository;

import com.nopain.manager.entity.WechatInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by zenof on 2017/11/22.
 */
public interface WechatInfoRepository  extends JpaRepository<WechatInfo,Long> {


    List<WechatInfo> findByAppId(String appId);

}
