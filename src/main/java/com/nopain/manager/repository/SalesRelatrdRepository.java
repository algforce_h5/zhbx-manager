package com.nopain.manager.repository;

import com.nopain.manager.entity.SalesRelatrd;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Glory
 * @version 1.0
 * @time 2017/11/24 11:06
 * @Description TODO 营销员信息操作
 */
public interface SalesRelatrdRepository extends JpaRepository<SalesRelatrd,Long> {
    SalesRelatrd findOneByPhone(String phone);
}
