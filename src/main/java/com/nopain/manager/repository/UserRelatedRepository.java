package com.nopain.manager.repository;

import com.nopain.manager.entity.UserRelated;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Glory
 * @version 1.0
 * @time 2017/11/24 09:58
 * @Description TODO
 */
public interface UserRelatedRepository extends JpaRepository<UserRelated,Long>{
    /**
     * 根据用户的手机号查询数据
     * @param phone
     * @return
     */
    UserRelated findOneByUserPhone(String phone);
}
