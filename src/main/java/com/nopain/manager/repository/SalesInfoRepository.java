package com.nopain.manager.repository;

import com.nopain.manager.entity.SalesInfo;
import com.nopain.manager.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 销售信息数据层
 */
public interface SalesInfoRepository extends JpaRepository<SalesInfo,Long> {

        List<SalesInfo> findBySalesmanId(String salesmanId);

        List<SalesInfo> findByThirdPartySysId(String openId);
}
