package com.nopain.manager.repository;

import com.nopain.manager.entity.SMSInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * 销售信息数据层
 */
public interface SMSInfoRepository extends JpaRepository<SMSInfo,Long> {

        List<SMSInfo> findByMoblPhonOrderByInsertTimeDesc(String phone);

}
