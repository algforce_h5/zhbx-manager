package com.nopain.manager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "algforce.ddh")
public class DDHSetting {

    public String startdate;

    public String enddate;

    private String smscode;

    private String env;

    private String staticresource;

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getSmscode() {
        return smscode;
    }

    public void setSmscode(String smscode) {
        this.smscode = smscode;
    }


    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getStaticresource() {
        return staticresource;
    }

    public void setStaticresource(String staticresource) {
        this.staticresource = staticresource;
    }
}
