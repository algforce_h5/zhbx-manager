package com.nopain.manager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Glory
 * @version 1.0
 * @time 2017/11/24 13:35
 * @Description TODO
 */
@ConfigurationProperties(prefix = "algforce.default")
public class DefaultSetting {
    private String reditUrl;

    public String getReditUrl() {
        return reditUrl;
    }

    public void setReditUrl(String reditUrl) {
        this.reditUrl = reditUrl;
    }
}
