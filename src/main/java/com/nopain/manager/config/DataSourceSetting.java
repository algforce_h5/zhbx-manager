package com.nopain.manager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by zenof on 2017/11/15.
 */
@ConfigurationProperties(prefix = "algforce.zh.datasource")
public class DataSourceSetting {

    private String usrname;

    private String pwd;

    private String dbk;

    public String getUsrname() {
        return usrname;
    }

    public void setUsrname(String usrname) {
        this.usrname = usrname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getDbk() {
        return dbk;
    }

    public void setDbk(String dbk) {
        this.dbk = dbk;
    }
}
