package com.nopain.manager.config;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arvin.Cao
 * @version 1.0
 * @time 16/3/18 下午6:43
 * @Description 响应操作, 用于请求完成后, 对响应数据的操作
 */
public class Response {
    private final static String MESSAGE = "message";//消息


    /**
     * 请求成功,无需返回结果集
     */
    public static ResponseEntity<Object> success() {
        Map<String, Object> result = new HashMap<String, Object>();
        return getEntity(result, HttpStatus.OK);
    }

    private static ResponseEntity<Object> getEntity(Object body,
                                                    HttpStatus statusCode) {
        MultiValueMap<String, String> headers = new HttpHeaders();
        List<String> contentType = new ArrayList<String>();
        contentType.add("application/json;charset=utf-8");
        headers.put("Content-Type", contentType);
        return new ResponseEntity<Object>(body, headers, statusCode);
    }

    /**
     * 请求成功,并返回请求结果集
     *
     * @param object 返回到客户端的对象
     * @return Spring mvc ResponseEntity
     */
    public static ResponseEntity<Object> success(Object object) {
        return getEntity(object, HttpStatus.OK);
    }

    /**
     * 服务器错误
     *
     * @param msg 请求失败的错误信息
     * @return Spring mvc ResponseEntity
     */
    public static ResponseEntity<Object> serverError(String msg) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(MESSAGE, msg);
        return getEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 请求失败,找不到数据
     *
     * @param msg 请求失败的错误信息
     * @return Spring mvc ResponseEntity
     */
    public static ResponseEntity<Object> notFound(String msg) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(MESSAGE, msg);
        return getEntity(result, HttpStatus.NOT_FOUND);
    }

    /**
     * 鉴权失败
     *
     * @param msg 请求失败的错误信息
     * @return Spring mvc ResponseEntity
     */
    public static ResponseEntity<Object> authorityFailed(String msg) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(MESSAGE, msg);
        return getEntity(result, HttpStatus.FORBIDDEN);
    }

    /**
     * 非法参数
     *
     * @param msg 请求失败的错误信息
     * @return Spring mvc ResponseEntity
     */
    public static ResponseEntity<Object> illegalArgument(
            String msg) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(MESSAGE, msg);
        return getEntity(result, HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * Bad Request
     *
     * @param msg
     * @return
     */
    public static ResponseEntity<Object> badRequest(int errorCode, String msg) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(MESSAGE, msg);
        return getEntity(result, HttpStatus.BAD_REQUEST);
    }


}
