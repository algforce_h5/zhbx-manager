package com.nopain.manager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信配置
 */
@ConfigurationProperties(prefix = "algforce.wechat")
public class WeChatSetting {

    private String domain;

    private String url;

    private String  code;

    private String enamecard;

    private String certificate;

    private String salesmansearch;

    private String appid;

    private String authorize;

    private Boolean on;

    private int calltime;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEnamecard() {
        return enamecard;
    }

    public void setEnamecard(String enamecard) {
        this.enamecard = enamecard;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getSalesmansearch() {
        return salesmansearch;
    }

    public void setSalesmansearch(String salesmansearch) {
        this.salesmansearch = salesmansearch;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAuthorize() {
        return authorize;
    }

    public void setAuthorize(String authorize) {
        this.authorize = authorize;
    }

    public Boolean getOn() {
        return on;
    }

    public void setOn(Boolean on) {
        this.on = on;
    }

    public int getCalltime() {
        return calltime;
    }

    public void setCalltime(int calltime) {
        this.calltime = calltime;
    }
}
