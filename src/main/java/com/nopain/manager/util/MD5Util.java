package com.nopain.manager.util;

import java.security.MessageDigest;


/**
 * Created by zenof on 2017/11/14.
 */
public class MD5Util {


    private static final String SALT = "manulife";

    public static String encode(String content) {
        content = content + SALT;
        MessageDigest md5 = null;
        String encodeStr = "";
        try {
            // 生成一个MD5加密计算摘要

            md5 = MessageDigest.getInstance("MD5");
            md5.update(content.getBytes("UTF-8"));
            encodeStr = byte2Hex(md5.digest()).toUpperCase();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return encodeStr;
    }


    /**
     * 将byte转为16进制
     * @param bytes
     * @return
     */
    private static String byte2Hex(byte[] bytes){
        StringBuffer stringBuffer = new StringBuffer();
        String temp = null;
        for (int i=0;i<bytes.length;i++){
            temp = Integer.toHexString(bytes[i] & 0xFF);
            if (temp.length()==1){
                //1得到一位的进行补0操作
                stringBuffer.append("0");
            }
            stringBuffer.append(temp);
        }
        return stringBuffer.toString();
    }
}
