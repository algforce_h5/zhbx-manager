package com.nopain.manager.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by zenof on 2017/11/15.
 */
public class DBUtil {

    public static String Decrypt(String text, String key) {
        Cipher cipher;
        String decryptedText = null;

        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes= new byte[16];
            byte[] b= key.getBytes("UTF-8");
            int len= b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.DECRYPT_MODE,keySpec,ivSpec);

            byte [] results = cipher.doFinal(Base64.decodeBase64(text));
            decryptedText = new String(results,"UTF-8");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return decryptedText;
    }

    public static String Encrypt(String text, String key) {
        Cipher cipher;
        String encryptedText = null;

        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes= new byte[16];
            byte[] b= key.getBytes("UTF-8");
            int len= b.length;
            if (len > keyBytes.length) len = keyBytes.length;
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.ENCRYPT_MODE,keySpec,ivSpec);

            byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
            encryptedText = Base64.encodeBase64URLSafeString(results);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encryptedText;
    }
}
