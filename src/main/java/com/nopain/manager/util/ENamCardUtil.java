package com.nopain.manager.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by zenof on 2017/11/14.
 */
public class ENamCardUtil {

    public static String getRequest(String salesmanId) {
        Date current = new Date();
        String reqDate = DateUtil.getDateStrByFormat(current, "yyyyMMdd");
        String reqTime = DateUtil.getTimeStrByFormat(current);
        String reqMac = MD5Util.encode(salesmanId);
        String format = "<eNameCardInfoReq xmlns=\"http://svc.manulife.com.cn/ifp/security/\">" +
                "  <dtlReq>" +
                "    <USERID>" + salesmanId + "</USERID>" +
                "  </dtlReq>" +
                "  <hdrReq>" +
                "    <ENCRYPT_TYPE>MD5</ENCRYPT_TYPE>" +
                "    <REQ_ALL_PAGE></REQ_ALL_PAGE>" +
                "    <REQ_CURR_PAGE></REQ_CURR_PAGE>" +
                "    <REQ_DATE>" + reqDate + "</REQ_DATE>" +
                "    <REQ_MAC>" + reqMac + "</REQ_MAC>" +
                "    <REQ_REC_NUM></REQ_REC_NUM>" +
                "    <REQ_TIME>" + reqTime + "</REQ_TIME>" +
                "    <REQ_TransNO>1000031</REQ_TransNO>" +
                "    <REQ_TransSource>11</REQ_TransSource>" +
                "  </hdrReq>" +
                "</eNameCardInfoReq>";
        return format;
    }

    public static Map<String, Object>  getVal(String content) throws DocumentException {
        if(StringUtils.isEmpty(content)) return null;
        Map<String, Object> data=new HashMap<String,Object>();
        data.put("sucflag", true);
        Document doc = DocumentHelper.parseText(content);
        Element root = doc.getRootElement();
        Element hdr =root.element("hdrResp");
        Element rspNoEelemt=hdr.element("RSP_RET_NO");
        String rspNo=rspNoEelemt.getText();
        if(!StringUtils.isEmpty(rspNo)) {
            return null;
//            data.put("sucflag", false);
//            data.put("message", "数据传输异常");
//            return data;
        }
        Element dtl=root.element("dtlResp");
        Element agtAddr=dtl.element("AgtAddr");
        data.put("address",agtAddr.getText().replace("<br/>",""));

//        Element agtComp=dtl.element("AgtComp");
        String agtCompStr="中宏人寿保险有限公司";
        data.put("company",agtCompStr);

        Element agtMobl=dtl.element("AgtMobl");
        data.put("phone",agtMobl.getText());

        Element agtName=dtl.element("AgtName");
        data.put("name",agtName.getText());

        Element agtPhone=dtl.element("AgtPhone");
        data.put("tel",agtPhone.getText());

        Element agtPhoto=dtl.element("AgtPhoto");
        data.put("photo",agtPhoto.getText());

        Element agtRank=dtl.element("AgtRank");
        data.put("rank",agtRank.getText());
        return data;
    }
}
