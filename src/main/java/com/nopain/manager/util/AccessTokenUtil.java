package com.nopain.manager.util;

import java.util.Date;

/**
 * Created by zenof on 2017/11/17.
 */
public class AccessTokenUtil  {

    private String appId;

    private String ticket;

    private Date expiredTime;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public Date getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(Date expiredTime) {
        this.expiredTime = expiredTime;
    }
}
