package com.nopain.manager.util;


import java.io.File;
import java.io.IOException;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.util.ResourceUtils;

public class AESUtil {

    public static final String ENCODING_UTF_8 = "UTF-8";

    public String encrypt(String data) throws Exception {
        String aesKey = this.getAESKey();
        return Base64.encode(this.encrypt(data.getBytes(ENCODING_UTF_8), Base64.decode(aesKey)));
    }

    public byte[] encrypt(final byte[] data, final byte[] key) throws Exception {
        // 还原密钥
        Key k = toKey(key);
        // 实例化
        Cipher cipher = Cipher.getInstance("AES");
        // 初始化,设置为加密模式
        cipher.init(Cipher.ENCRYPT_MODE, k);
        // 执行操作
        return cipher.doFinal(data);
    }

    public String decrypt(String data) throws Exception {
        String aesKey = this.getAESKey();
        return new String(this.decrypt(Base64.decode(data), Base64.decode(aesKey)), ENCODING_UTF_8);
    }

    public byte[] decrypt(final byte[] data, final byte[] key) throws Exception {
        // 还原密钥
        Key k = toKey(key);
        // 实例化
        Cipher cipher = Cipher.getInstance("AES");
        // 初始化,设置为解密模式
        cipher.init(Cipher.DECRYPT_MODE, k);
        // 执行操作
        return cipher.doFinal(data);
    }

    public Key toKey(final byte[] key) throws Exception {
        // 实例化AES密钥
        SecretKey secretKey = new SecretKeySpec(key, "AES");
        return secretKey;
    }

    public String getAESKey() throws IOException {
        //    	String privateKeyFile = "D:\\mm\\aes_key.keystore";
//        String privateKeyFile = AESUtil.class.getClassLoader().getResource("/dbs").getPath() + File.separator + "aes_key.keystore";
        return FileUtils.readFileToString(ResourceUtils.getFile("classpath:security/encrypt_key.txt"));
    }
}

