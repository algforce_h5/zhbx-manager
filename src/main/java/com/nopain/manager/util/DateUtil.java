package com.nopain.manager.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zenof on 2017/10/29.
 */
public class DateUtil {

    public static final int daysBetween(Date early, Date late) {

        java.util.Calendar calst = java.util.Calendar.getInstance();
        java.util.Calendar caled = java.util.Calendar.getInstance();
        calst.setTime(early);
        caled.setTime(late);
        //设置时间为0时
        calst.set(java.util.Calendar.HOUR_OF_DAY, 0);
        calst.set(java.util.Calendar.MINUTE, 0);
        calst.set(java.util.Calendar.SECOND, 0);
        caled.set(java.util.Calendar.HOUR_OF_DAY, 0);
        caled.set(java.util.Calendar.MINUTE, 0);
        caled.set(java.util.Calendar.SECOND, 0);
        //得到两个日期相差的天数
        int days = ((int) (caled.getTime().getTime() / 1000) - (int) (calst
                .getTime().getTime() / 1000)) / 3600 / 24;

        return days;
    }

    public static long  getTime(Date date){
        java.util.Calendar calst = java.util.Calendar.getInstance();
        calst.setTime(date);
        return calst.getTime().getTime();
    }

    public static Date getDate(String val) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date target = df.parse(val);
        return target;
    }

    public static String getDateStrByFormat(Date date,String format){
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static String getTimeStrByFormat(Date date){
        DateFormat df = new SimpleDateFormat("HHmmss");
        return df.format(date);
    }
}
