package com.nopain.manager.util;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.util.ResourceUtils;


public class RSAUtil {

	private static final String RSA_PUBLIC_KEY_FILE = "rsa_public_key_file";
	private static final String RSA_PRIVATE_KEY_FILE = "rsa_private_key_file";
	/**
	 * 签名算法
	 */
	public static final String SIGN_ALGORITHMS = "SHA1WithRSA";

	public String sign(String content) throws GeneralSecurityException, IOException {
		String privateKey = this.getPrivateKey();
		return this.sign(content, privateKey);
	}

	public String sign(String content, String privateKey) throws GeneralSecurityException {
		byte[] sha1Data = SecureUtil.sha1X16(content, "UTF-8");
		System.out.println(sha1Data);
		System.out.println("SHA1zhaiyao:" + new String(sha1Data));
		PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(privateKey));
		KeyFactory keyf = KeyFactory.getInstance("RSA");
		PrivateKey priKey = keyf.generatePrivate(priPKCS8);
		java.security.Signature signature = java.security.Signature.getInstance(SIGN_ALGORITHMS);
		signature.initSign(priKey);
		signature.update(sha1Data);
		byte[] signed = signature.sign();
		return Base64.encode(signed);
	}

	public boolean verify(String content, String signData) throws GeneralSecurityException, IOException {
		String publicKey = this.getPublicKey();
		return this.verify(content, signData, publicKey);
	}

	public boolean verify(String content, String signData, String publicKeyStr) throws GeneralSecurityException {
		byte[] sha1Data = SecureUtil.sha1X16(content, "UTF-8");
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decode(publicKeyStr));
		PublicKey publicKey = keyFactory.generatePublic(keySpec);
		Signature verifier = Signature.getInstance(SIGN_ALGORITHMS);
		verifier.initVerify(publicKey);
		verifier.update(sha1Data);
		return verifier.verify(Base64.decode(signData));
	}

	public String getPrivateKey() throws IOException {
//        String privateKeyFile = "D:/Desktop/rsa_private_key.keystore";
		// String privateKeyFile = RSAUtil.class.getClassLoader().getResource("/open").getPath()+File.separator +
		// "rsa_private_key.keystore";
		return FileUtils.readFileToString(ResourceUtils.getFile("classpath:security/wechatCampaign3_private_key.txt"));
	}

	public String getPublicKey() throws IOException {
//        String publicKeyFile = "D:/Desktop/rsa_public_key.keystore";
		// String publicKeyFile = RSAUtil.class.getClassLoader().getResource("/open").getPath()+File.separator +
		// "rsa_public_key.keystore";
		return FileUtils.readFileToString(ResourceUtils.getFile("classpath:security/wechatCampaign3_public_key.txt"));
	}

//    public static void main(String[] args) throws Exception {
//		/*Map json = new HashMap<String, String>();
//		json.put("state", "NBA5v5");
//		String data = JsonUtil.EntityToJson(json);
//		//data = "{\"state\":\"NBA5v5\"}";
//		data = "{\"authorize_callback_domain\":\"\",\"task_token\":\"1\",\"description\":\"sit\","
//				+ "\"jsaip_security_domain_1\":\"\",\"token_update_time\":\"1438136986390\","
//				+ "\"appid\":\"wxfc11759ac82d48f3\",\"jsaip_security_domain_3\":\"\",\"jsaip_security_domain_2\":\"\","
//				+ "\"jsapi_ticket_update_time\":\"1438137581073\",\"jsapi_ticket\":\"kgt8ON7yVITDhtdwci0qebtKJVzWTSqIapX_MRViB663qgckbBIIP0Secrfk02ucVBn53fbmcu1yn5juQnuPNw\","
//				+ "\"task_ticket\":\"0\",\"access_token\":\"xiMmHQk-LJhBCxSlgpopM3GHfsGqO8IQ1vJJPUiOBTWVFdCzGdJDC3nZfH1vFiDnRvL_TNHUPzOSBoIgrBgvEhGW5uiCezeoGIyVvyNXLBzjDXPEBjRxj8lUp_wyBEkhTAQbAAAAHX\"}";
//		System.out.println(data);*/
//
//        //String data = "{\"expire_seconds\": 604800, \"action_name\": \"QR_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": \"test\"}}}";
//        //String data = "{\"touser\":\"OPENID\",\"msgtype\":\"text\",\"text\":{\"content\":\"Hello World\"}}";
//        //String data ="{\"title\":\"标题\",\"introduction\":\"这是一个描述\"}";
///*    	String data = "{\"expire_seconds\": 604800, \"action_name\": \"QR_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": \"test\"}}}";
//		RSAUtil rsaUtil = new RSAUtil();
//		String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALzP376EZPFqfMHi/comwtHtjPkXrkewymCU/UYR0XDjPOD/jP+3odG0mCIixLP9TjUxVCoLnNR2Byed+P66AtwI1Ax4ptRfOx+i6b5vDt+QLrS2S/3XAUCvXd39baNm9ZSpsbEmtFTFtGz8O8bW8teS9sCARpDOGicHpwRi33F/AgMBAAECgYAqPzkJIAaWHgWcPHYc7V9aKZUJh0rvr5cY1T12mDYM1Eui7ItYDHagvY/s02q/+0WskUULw11sDJy9OJOmK2jIAJm83qvlN0ebatsBFACC5y/3jOCB2iQ5rL5aJkbnqDWPGgwYJwKCwWg6x82ThBVlbz2hjZEujv1cFmeEtv3mAQJBAOT/M47kCoZ98pKHVkH2e5bVYIYNLxQ+yZA74TgDbYUA6P2CKt1NjwTc+VtL/4q/vyweo4zJKIGT3fqWpqb/IB8CQQDTE5l/4ki+Zfrh55NZVx4l/ZfXAB8o4szbZXaddAXave9qWWJ6p9Lz59vQ1ZqTsMOvjfBX2+LrQGaX9uSsVAKhAkEA3lcrKEIiNhNzwvHEvqcjuH/VCFMksvODoZeCRXCOleUcU4hfNiBoDMDBG/PeWklyxNO62Kc3vRhlHznG9b4g4QJBAK01ESo68zyijLxh/q06avf74GTCqc8wRbDn0u0j70XvxI49qS1DmD0kD2KQVhdJ31qpHAfM3an0n/ul00KCceECQDPnqYx84mTrD0RySrTtYhG1QlmHfRdTBUTaRtUEO86c+/oRXKJ623x6Kc7xppNmySsFRWgTOJCBoXP7Oxr9xPg=";
//		// String privateKey = rsaUtil.getPrivateKey();
//		//String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDE5DcVTBkh0LGdYWkDEpw0aZi8YBSpZAoczts/Cl8KuYgHKH3y6J88JAMgwevBjQCusaSxEvgjxZGqFEhOv2bm5J91/RSye8MqZvLhnCk+fgrkmFto2pjCNREG3MZ/Mk715MlHn24fb9gwAUSTJSRn/89NeOnsJyZC/8yI7vL87wIDAQAB";
//		String publicKey ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8z9++hGTxanzB4v3KJsLR7Yz5F65HsMpglP1GEdFw4zzg/4z/t6HRtJgiIsSz/U41MVQqC5zUdgcnnfj+ugLcCNQMeKbUXzsfoum+bw7fkC60tkv91wFAr13d/W2jZvWUqbGxJrRUxbRs/DvG1vLXkvbAgEaQzhonB6cEYt9xfwIDAQAB";
//		// String publicKey = rsaUtil.getPublicKey();
//    	System.out.println(privateKey.length());
//    	System.out.println(publicKey.length());
//
//    	String sign = "pKAXDqCFtFRoJIMVM0H/vntdnmuM0R7+a4K2q/RYRFemgXElBG5qd9O3kjfbzibUSEWQoSytOE2gkcMauBnRE+rGWwxpqcbVgItw/wcP+GWWnljtcJ4bLmPKFBb52sZPrLr3Iua6/j4F7ewiMZV41wfr1cUhhEFOva+BB56YxGA=";
//		String sign1 = rsaUtil.sign(data, privateKey);
//		System.out.println("加密后："+sign1);
//		System.out.println("加密后是否相等："+sign1.equals(sign));
//		boolean verify = rsaUtil.verify(data, sign, publicKey);
//		System.out.println("verify = " + verify);
//		*/
//        RSAUtil rsaUtil = new RSAUtil();
//        //String test="SW/aFF3FChjCUdr5phM4jCxro/A/kcXopWo9Jdkzjc8ppimSeSy2kxxiwXnvwyUCUsrlbli7FfUFAXb0i//DCdxLdWgjU9aQ0aQQ/uOQ8uktVV9SH2iXLqCOMRR+NmhBeJ2TLhJlulgXcQLL519wov0okbVG/t0trauThS4+OFud0FsVic2oN7UXLpEvX6pW/f0vJpHJcoskiCXy0KLoZVKllmFvZMPM3FHxyZprQHmsbNGygZive3Tql8Qq6DxwDlSEfQuYauJ0Qr5AO8m6hy9tRHEPjzUtxp4DXianafVNdbsT4287rNG/76xG8Mje08EydlTWcMZAi1xmjM/jI2lAFwlN0VwxLK9gbdsid6NrSB4At0wyntu5C0RZo+4Qjk/34A1/ZiHwRtUdbdr5HUPNl6oYzXr6XzvbuH8nZgcqXTD/XY3gyDge5kT4PeNf+Wux1gZ6Nd6Jo/qmuzd1QE1bE0VQ4V/HKDwNgz2sbeHfNUBYzH0u5+R52rQh0wz5";
//        String test = "SW/aFF3FChjCUdr5phM4jH/lHpLjFXA4cs/hOu5alFvceIOVe1g04q3R9uRvd8qdAXNGZxtsVzwl2S54ypVgrFak8HohdjTC8k4kWe3a32QuAy5PjtJcjEd9W7OdPmNZJI9cFgiLH+Q3l0pVFXtokUTLF4j4S6A8u0AuzH8JTbU59R/dzhUztOFenSPBunNunrzH6nBLGWImm9KL9TFKtIsYAveEh2iSgIROFfEuTcAvCYvdFAvapprRGsyQAkyGodZjT8QXJbZqckbwCSnmsqiELvuF1Q1wHdnVgEn9nwG1SJIpIp6nNOwIVU8IEH/oK1iF0MQTHqddJQYN1MSqx9MIFlC7rxUFGNM/YZAIq9sW+4farEeMH/gHCZpjhYvVVSkPAjStA7V4JyUMFh0yO2Ar6xnw3WN3WdCp6KjIl4Ii1CN94MW0uOP48liaQCxiHkGPp2uD5dzIZKvWxod3aXW5yFiG02/qg2E9LV7OXaEA5Iu42DMDGA3Q56u4EGJFoCS1u6J7ZyVXpQMvTuN47Q==";
//        AESUtil aesUtil = new AESUtil();
//        String encryptKey = "dZDRFD0pX223vAfAZ4alww==";
//        String decryptData = new String(aesUtil.decrypt(Base64.decode(test), Base64.decode(encryptKey)), AESUtil.ENCODING_UTF_8);
//        System.out.println(decryptData);
//
//	/*	String datas="{\"subscribe\":1,\"openid\":\"ofvBGv7CMvtKMnuGt-sZ_2jDjFzI\",\"nickname\":\"亮\",\"sex\":1,\"language\":\"zh_CN\",\"city\":\"浦东新区\",\"province\":\"上海\",\"country\":\"中国\",\"headimgurl\":"
//				+ "\"http:\\\\/\\\\/wx.qlogo.cn\\\\/mmopen\\\\/XxT9TiaJ1ibf2j28QuDlia5vuTX2PRMrw3t0AUh0qK73HJNiapfmDS58NJR0Zmx3rGP6WtPynrhh4eJ7f8Qw8HHxwQuia08mTpk8o\\\\/0\",\"subscribe_time\":1503986467,\"remark\":\"\",\"groupid\":0,\"tagid_list\":[]}";
//		String signs = "bE5Wi8rvfHj+x/W9mhU97kfDgp9DIrxuSCLlxHpDOb4bVb5v4XsoJUwg2dVwTfvKNE6UMSVdKX1Qc8ojMqObxZAk18k8Qh6pY8nnMzSh0RSoPQqT0MeqR90MoaBh5rE0O7dQpihgYg8UM9WRotvuB3mq3nHbNkd6tD5a8vDrA58=";
//		*/
//        String datas = "{\"state\":\"wrp01\"}";
////        String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALzP376EZPFqfMHi/comwtHtjPkXrkewymCU/UYR0XDjPOD/jP+3odG0mCIixLP9TjUxVCoLnNR2Byed+P66AtwI1Ax4ptRfOx+i6b5vDt+QLrS2S/3XAUCvXd39baNm9ZSpsbEmtFTFtGz8O8bW8teS9sCARpDOGicHpwRi33F/AgMBAAECgYAqPzkJIAaWHgWcPHYc7V9aKZUJh0rvr5cY1T12mDYM1Eui7ItYDHagvY/s02q/+0WskUULw11sDJy9OJOmK2jIAJm83qvlN0ebatsBFACC5y/3jOCB2iQ5rL5aJkbnqDWPGgwYJwKCwWg6x82ThBVlbz2hjZEujv1cFmeEtv3mAQJBAOT/M47kCoZ98pKHVkH2e5bVYIYNLxQ+yZA74TgDbYUA6P2CKt1NjwTc+VtL/4q/vyweo4zJKIGT3fqWpqb/IB8CQQDTE5l/4ki+Zfrh55NZVx4l/ZfXAB8o4szbZXaddAXave9qWWJ6p9Lz59vQ1ZqTsMOvjfBX2+LrQGaX9uSsVAKhAkEA3lcrKEIiNhNzwvHEvqcjuH/VCFMksvODoZeCRXCOleUcU4hfNiBoDMDBG/PeWklyxNO62Kc3vRhlHznG9b4g4QJBAK01ESo68zyijLxh/q06avf74GTCqc8wRbDn0u0j70XvxI49qS1DmD0kD2KQVhdJ31qpHAfM3an0n/ul00KCceECQDPnqYx84mTrD0RySrTtYhG1QlmHfRdTBUTaRtUEO86c+/oRXKJ623x6Kc7xppNmySsFRWgTOJCBoXP7Oxr9xPg=";
////        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8z9++hGTxanzB4v3KJsLR7Yz5F65HsMpglP1GEdFw4zzg/4z/t6HRtJgiIsSz/U41MVQqC5zUdgcnnfj+ugLcCNQMeKbUXzsfoum+bw7fkC60tkv91wFAr13d/W2jZvWUqbGxJrRUxbRs/DvG1vLXkvbAgEaQzhonB6cEYt9xfwIDAQAB";
//        String privateKey=rsaUtil.getPrivateKey();
//        String publicKey=rsaUtil.getPublicKey();
//        //decryptData= "{\"media_id\":\"正解为了忘记了\"}";
//        String enc = rsaUtil.sign(datas, privateKey);
//        System.out.println("RSA加密后：" + enc);
//        boolean a = rsaUtil.verify(decryptData, enc, publicKey);
//        System.out.println(a);
//
//
//    }
}