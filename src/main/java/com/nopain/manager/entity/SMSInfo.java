package com.nopain.manager.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by zenof on 2017/11/14.
 */
@Entity
@Table(name="rectmt_sms_trxn")
public class SMSInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String moblPhon;

    private String msgType;

    private String msgTxt;

    private String senderId;

    private Date insertTime;

    private String statCd;

    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="mobl_phon",length = 40)
    public String getMoblPhon() {
        return moblPhon;
    }

    public void setMoblPhon(String moblPhon) {
        this.moblPhon = moblPhon;
    }

    @Column(name="msg_type",length = 50)
    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @Column(name="msg_txt",length = 500)
    public String getMsgTxt() {
        return msgTxt;
    }

    public void setMsgTxt(String msgTxt) {
        this.msgTxt = msgTxt;
    }

    @Column(name="sender_id",length = 20)
    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    @Column(name="insert_time")
    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    @Column(name="stat_cd")
    public String getStatCd() {
        return statCd;
    }

    public void setStatCd(String statCd) {
        this.statCd = statCd;
    }

    @Column(name="code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}


