package com.nopain.manager.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Glory
 * @version 1.0
 * @time 2017/11/24 11:01
 * @Description TODO 绑定营销员信息时使用的表
 */

@Entity
public class SalesRelatrd {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * 营销员id
     */
    private String salesmanId;
    /**
     * 营销员name
     */
    private String name;
    /**
     * code
     */
    private String salesmanCode;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 营销员手机号
     */
    private String phone;
    /**
     * 省份
     */
    private String province;

    public SalesRelatrd() {
    }

    public SalesRelatrd(String salesmanId, String name, String salesmanCode, String phone, String province) {
        this.salesmanId = salesmanId;
        this.name = name;
        this.salesmanCode = salesmanCode;
        this.phone = phone;
        this.province = province;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalesmanCode() {
        return salesmanCode;
    }

    public void setSalesmanCode(String salesmanCode) {
        this.salesmanCode = salesmanCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
