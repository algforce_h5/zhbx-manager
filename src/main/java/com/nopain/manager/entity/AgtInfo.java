package com.nopain.manager.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by zenof on 2017/11/23.
 */
@Entity
public class AgtInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String salesmenId;

    private String agtPhoto;

    private String agtCD;

    private String agtName;

    private String agtRank;

    private String agtAddress;

    private String agtCompany;

    private String agtTel;

    private String agtMobile;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgtPhoto() {
        return agtPhoto;
    }

    public void setAgtPhoto(String agtPhoto) {
        this.agtPhoto = agtPhoto;
    }

    public String getSalesmenId() {
        return salesmenId;
    }

    public void setSalesmenId(String salesmenId) {
        this.salesmenId = salesmenId;
    }

    public String getAgtCD() {
        return agtCD;
    }

    public void setAgtCD(String agtCD) {
        this.agtCD = agtCD;
    }

    public String getAgtName() {
        return agtName;
    }

    public void setAgtName(String agtName) {
        this.agtName = agtName;
    }

    public String getAgtRank() {
        return agtRank;
    }

    public void setAgtRank(String agtRank) {
        this.agtRank = agtRank;
    }

    public String getAgtAddress() {
        return agtAddress;
    }

    public void setAgtAddress(String agtAddress) {
        this.agtAddress = agtAddress;
    }

    public String getAgtCompany() {
        return agtCompany;
    }

    public void setAgtCompany(String agtCompany) {
        this.agtCompany = agtCompany;
    }

    public String getAgtTel() {
        return agtTel;
    }

    public void setAgtTel(String agtTel) {
        this.agtTel = agtTel;
    }

    public String getAgtMobile() {
        return agtMobile;
    }

    public void setAgtMobile(String agtMobile) {
        this.agtMobile = agtMobile;
    }
}
