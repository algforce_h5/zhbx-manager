package com.nopain.manager.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by zenof on 2017/11/14.
 */
@Entity
public class SalesInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String salesmanId;

    private String salesmanCode;

    private String phoneSuffix;

    private String province;

    private String city;

    private String staffType;

    private String thirdPartySystem;

    private String thirdPartySysCode;

    private String thirdPartySysId;

    private String thirdPartySysDesc;

    private String bindingStatus;

    private Long bindingTime;

    private Long createTime;

    private Long modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanCode() {
        return salesmanCode;
    }

    public void setSalesmanCode(String salesmanCode) {
        this.salesmanCode = salesmanCode;
    }

    public String getPhoneSuffix() {
        return phoneSuffix;
    }

    public void setPhoneSuffix(String phoneSuffix) {
        this.phoneSuffix = phoneSuffix;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStaffType() {
        return staffType;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getThirdPartySystem() {
        return thirdPartySystem;
    }

    public void setThirdPartySystem(String thirdPartySystem) {
        this.thirdPartySystem = thirdPartySystem;
    }

    public String getThirdPartySysCode() {
        return thirdPartySysCode;
    }

    public void setThirdPartySysCode(String thirdPartySysCode) {
        this.thirdPartySysCode = thirdPartySysCode;
    }

    public String getThirdPartySysId() {
        return thirdPartySysId;
    }

    public void setThirdPartySysId(String thirdPartySysId) {
        this.thirdPartySysId = thirdPartySysId;
    }

    public String getThirdPartySysDesc() {
        return thirdPartySysDesc;
    }

    public void setThirdPartySysDesc(String thirdPartySysDesc) {
        this.thirdPartySysDesc = thirdPartySysDesc;
    }

    public String getBindingStatus() {
        return bindingStatus;
    }

    public void setBindingStatus(String bindingStatus) {
        this.bindingStatus = bindingStatus;
    }

    public Long getBindingTime() {
        return bindingTime;
    }

    public void setBindingTime(Long bindingTime) {
        this.bindingTime = bindingTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public void copyTo(SalesInfo target){
        target.setSalesmanCode(this.getSalesmanCode());
        target.setPhoneSuffix(this.getPhoneSuffix());
        target.setProvince(this.getProvince());
        target.setCity(this.getCity());
        target.setStaffType(this.getStaffType());
        target.setThirdPartySystem(this.getThirdPartySystem());
        target.setThirdPartySysCode(this.getThirdPartySysCode());
        target.setThirdPartySysId(this.getThirdPartySysId());
        target.setThirdPartySysDesc(this.getThirdPartySysDesc());
        target.setBindingStatus(this.getBindingStatus());
        target.setBindingTime(this.getBindingTime());
        target.setCreateTime(this.getCreateTime());
        target.setModifyTime(this.getModifyTime());
    }
}
