package com.nopain.manager.entity.form;

import java.io.Serializable;

/**
 * @author Glory
 * @version 1.0
 * @time 2017/11/24 10:08
 * @Description TODO 绑定营销员的表单
 *
 */
public class RelatrdForm implements Serializable{
    /**
     * 营销员的phone
     */
    private String salesmanphone;
    /**
     * 用户的phone(加密之后的)
     */
    private String userphone;

    public String getSalesmanphone() {
        return salesmanphone;
    }

    public void setSalesmanphone(String salesmanphone) {
        this.salesmanphone = salesmanphone;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }
}
