package com.nopain.manager.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * 登入信息
 */
@Entity
public class LoginInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * 登入时间
     */
    private Date createTime;

    /**
     * openid
     */
    private String openId;

    /**
     * 父openId
     */
    private String parentOpenId;

    /**
     * 营销员唯一码
     */
    private String salesmanId;

    /**
     * 营销员编号
     */
    private String salesmanCode;

    /**
     * 营销员oepnId
     */
    private String salesmanOpenId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getParentOpenId() {
        return parentOpenId;
    }

    public void setParentOpenId(String parentOpenId) {
        this.parentOpenId = parentOpenId;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanCode() {
        return salesmanCode;
    }

    public void setSalesmanCode(String salesmanCode) {
        this.salesmanCode = salesmanCode;
    }

    public String getSalesmanOpenId() {
        return salesmanOpenId;
    }

    public void setSalesmanOpenId(String salesmanOpenId) {
        this.salesmanOpenId = salesmanOpenId;
    }
}
